# Wildcard characters

## . character

The period (.) symbol is used to represent the current directory in a file path. For example, "./file.txt" represents a file named "file.txt" in the current directory.

## .. Charcter
The double period (..) is used to represent the parent directory to the corrent working directory.For example if current working directory is /home/videos then parent directory refers to home


## ~ Character

Tilde character is used to change our current working directory to home directory

## ^ Character

Caret character is used to match the lines that are starting from that string
eg: ^harry gives lines that started with harry in a text file 

## $ symbol

Dollar symbol is used to match the lines that are starting from that string
eg: harry$ gives lines that ends with harry in a text file

## * Character

Asterisk character is used to match zero or more characters in a file name

## ? symbol

This symbol is used to match one character in a file name 

